#### General description
The goal of this port is to run Arduino Sketches on Linux.
At the moment this port is limited to the HardwareSerial, Stream and Print classes
and the time functions.
The input and output of HardwareSerial is stdin and stdout.


#### Features
- Sample Makefile for compiling a Sketch
- Sample Sketch (ASCIITable.ino)


#### Limitations
- There is no serialEvent() member in HardwareSerial.
- Only tested with ASCIITable.ino.
- There is always a `\n` in read functions. I think a serial console does not send newlines?


#### Building
- To build the sample Sketch just type `make`.
- If you want to test another Sketch, replace it with ASCIITable.ino and change the Name variable in the Makefile. Then again build it with `make`.