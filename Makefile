NAME = ASCIITable

CC = c++
LDFLAGS = -pthread
SRC_PATH = ArduinoCore-Linux/cores/arduino/
CFLAGS := -I$(SRC_PATH)
SRC := $(wildcard $(SRC_PATH)*.cpp) 
OBJ = $(patsubst %.cpp,%.o,$(SRC))


$(NAME): $(OBJ) $(NAME).o
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)


$(NAME).o: $(NAME).ino
	$(CC) -c -o $@ -include $(SRC_PATH)Arduino.h -x c++ $^ $(CFLAGS) $(LDFLAGS)


%.o: %.cpp
	$(CC) -c -o $@ $< $(CFLAGS)


clean:
	rm $(OBJ) $(NAME) $(NAME).o
