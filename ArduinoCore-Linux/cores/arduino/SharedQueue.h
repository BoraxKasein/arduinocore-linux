#ifndef SharedQueue_H__
#define SharedQueue_H__

#include <queue>
#include <mutex>

class SharedQueue {
private:
    std::queue<int> q;
    std::mutex m;
public:
    void push(int x);
    int pop();
    int first();
    int size();
};

#endif // SharedQueue_H__
