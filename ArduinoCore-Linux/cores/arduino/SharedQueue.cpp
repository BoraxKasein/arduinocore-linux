#include "SharedQueue.h"

void SharedQueue::push(int x) {
    m.lock();
    q.push(x);
    m.unlock();
}


int SharedQueue::pop() {
    int x = -1;

    m.lock();
    if (!q.empty()) {
        x = q.front();
        q.pop();
    }
    m.unlock();

    return x;
}


int SharedQueue::first() {
    int x = -1;

    m.lock();
    if (!q.empty())
        x = q.front();
    m.unlock();

    return x;
}


int SharedQueue::size() {
    int s;

    m.lock();
    s = q.size();
    m.unlock();

    return s;
}
